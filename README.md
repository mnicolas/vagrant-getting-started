# Getting started with Vagrant

This project is a playground learn using [Vagrant](https://developer.hashicorp.com/vagrant).

The goal is to be able to automate the provisionning of several VMs, to model a distributed system, and to deploy a multi-component application on them.

## Ressources

- https://developer.hashicorp.com/vagrant/tutorials/getting-started